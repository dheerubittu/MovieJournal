#!/usr/bin/env python3

from os.path import join
from distutils.core import setup

setup(
    name='moviejournal',
    version='0.1',
    scripts=[join('bin', 'moviejournal'), join('bin', 'helper.py')]
    )
