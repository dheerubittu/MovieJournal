#ifndef MJ_HPP
#define MJ_HPP
#include <qt6/QtWidgets/QPushButton>
#include <qt6/QtWidgets/QLineEdit>
#include <qt6/QtWidgets/QLabel>
#include <qt6/QtWidgets/QTableView>
#include <qt6/QtCore/QFile>
#include <qt6/QtWidgets/QCompleter>
#include <qt6/QtWidgets/QMenuBar>
#include <qt6/QtWidgets/QStatusBar>
#include <qt6/QtWidgets/QVBoxLayout>
#include <qt6/QtGui/QStandardItem>
#include <qt6/QtCore/QStandardPaths>
#include <qt6/QtWidgets/QTableWidget>
#include <qt6/QtWidgets/QTableWidgetItem>
#include <qt6/QtWidgets/QWidget>
#include <qt6/QtSql/QSqlDatabase>
#include <qt6/QtSql/QSqlQuery>
#include <qt6/QtSql/QSqlRecord>
#include <qt6/QtCore/QDir>
#include <qt6/QtSql/QSqlTableModel>
#include <qt6/QtCore/QModelIndex>
#include <qt6/QtWidgets/QHBoxLayout>
#include <qt6/QtWidgets/QCheckBox>
#include <qt6/QtWidgets/QDoubleSpinBox>
#include <qt6/QtWidgets/QMessageBox>
#include <qt6/QtWidgets/QGridLayout>
#include <qt6/QtCore/QSortFilterProxyModel>
#include <qt6/QtCore/QItemSelectionModel>
#include <qt6/QtWidgets/QHeaderView>
#include <qt6/QtWidgets/QDialog>
#include <qt6/QtWidgets/QMainWindow>
#include <qt6/QtWidgets/QApplication>
#include <qt6/QtGui/QKeySequence>
#include <qt6/QtCore/QDebug>
#include <qt6/QtGui/QCloseEvent>
#include <qt6/QtGui/QPixmap>
#include <qt6/QtGui/QShortcut>
#include <qt6/QtWidgets/QSizePolicy>

static const QString qconfdir = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
static const QString AppConfDir = qconfdir + "/moviejournal/";
class ratingSpinBox : public QDoubleSpinBox
{
    public:
        ratingSpinBox() {
            this->setMinimum(0);
            this->setMaximum(11);
            this->setSingleStep(0.25);
        }
        ~ratingSpinBox() {}
};
class MainWindow : public QMainWindow
{
    public:
        QShortcut *deselect_table;
        QMenuBar *menubar;
        QStatusBar *statusbar;
        QMenu *filemenu, *editmenu, *addmenu;
        QAction *exit, *newAction, *editAction, *saveAction, *saveAsAction, *prefAction, *undoAction, *redoAction, *single_action, *multiple_action, *later_action;
        QSortFilterProxyModel *sort_model;
        QSqlDatabase db;
        QSqlTableModel *model;
        QLabel *movie_count, *movie_rating_meter;
        QVBoxLayout *main_layout;
        QHBoxLayout *search_layout, *statusbar_layout;
        QWidget *main_widget, *statusbar_widget;
        QTableView *table;
        QLineEdit *search;
        QPushButton *addbtn, *editbtn, *savebtn, *deletebtn;
        MainWindow();
        ~MainWindow() {}

        void init_table();
        void init_menubar();
        void init_statusbar();
        void search_text(QString text);
        void save_all();
        void closeEvent(QCloseEvent *e);
        void deleteEntry();
        void update_movie_count();
        void styleSheet();
};

class Skeleton : public QWidget
{
    public:
        QCheckBox *nowatch;
        QDoubleSpinBox *rating;
        QGridLayout *main_layout;
        QPushButton *addbtn, *cancel_btn;
        QLineEdit *title;
        QHBoxLayout *review_meter_layout;
        QLabel *title_label, *rating_label, *review_meter;
        Skeleton();
        ~Skeleton() {}
        void review_meter_function(double);
        void styleSheet();
};

class AddWindow : public Skeleton
{
    public:
        AddWindow(MainWindow *w);
        ~AddWindow() {}
        void add(MainWindow *w);
};

class AddMultipleWindow : public QWidget
{
    public:
        QString title, rating;
        QStringList titles, ratings;
        QSqlTableModel *m;
        bool no_title_error = false;
        ratingSpinBox *spin_box;
        QTableWidget *mult_table;
        QVBoxLayout *main_layout;
        QWidget *addNewWidget;
        QHBoxLayout *btnlayout;
        QPushButton *done_btn, *cancel_btn, *addrowBtn, *delrowBtn;
        AddMultipleWindow(MainWindow *w);
        ~AddMultipleWindow() {}
        void review_meter_function(ratingSpinBox *r, double);
        void done(MainWindow *w);
        void styleSheet();
};

class EditWindow : public Skeleton
{
    public:
        QSqlTableModel *M;
        QSortFilterProxyModel *MM;
        QModelIndex i;
        QSqlRecord r;
        EditWindow(MainWindow *w);
        ~EditWindow() {}
        void add();
};

class MessageBox : public QMessageBox
{
    public:
        std::string s = R"(
            QMessageBox {
                background-color: #1E3440;
                min-width: 500px;
                min-height: 200px;
            }

            QMessageBox QLabel {
                font-size: 17px;
                font-family: "Rajdhani Medium";
                color: #D4D4D4;
            }

            QMessageBox QPushButton {
                font-size: 17px;
                font-family: "Rajdhani Medium";
                background-color: #F35000;
            }
        )";
        MessageBox(QString text) {
            this->setText(text);
            this->setStyleSheet(QString::fromStdString(s));
            this->exec();
        }
};

#endif
