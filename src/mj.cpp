#ifndef MJ_CPP
#define MJ_CPP
#include "mj.hpp"

class MySortFilterProxyModel : public QSortFilterProxyModel
{

    public:
        QVariant data(const QModelIndex &ind, int role) const {
            QVariant value = QSortFilterProxyModel::data(ind, role);
            if(role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            return value;
        }
        bool lessThan(const QModelIndex &left, const QModelIndex &right) const {
            return left.data().toFloat() < right.data().toFloat();
        }
};

MainWindow::MainWindow()
{
    this->setMinimumSize(700, 500);
    statusbar = new QStatusBar();
    init_menubar();
    search_layout->addWidget(search);
    search_layout->addWidget(addbtn);
    search_layout->addWidget(editbtn);
    search_layout->addWidget(savebtn);
    search_layout->addWidget(deletebtn);

    main_layout->addLayout(search_layout);
    main_layout->addWidget(table);
    main_widget->setLayout(main_layout);
    main_layout->setContentsMargins(0, 0, 0, 0);
    setCentralWidget(main_widget);
    setStatusBar(statusbar);
    init_table();
    init_statusbar();
    connect(table->selectionModel(), &QItemSelectionModel::selectionChanged, this, [this] {
        if(table->selectionModel()->hasSelection()) {
            editbtn->setDisabled(false);
            deletebtn->setDisabled(false);
        }
        else {
            editbtn->setDisabled(true);
            deletebtn->setDisabled(true);
        }
    });
    styleSheet();
}
void MainWindow::styleSheet()
{
    std::string s = R"(
        QMessageBox {
            background-color: #1E3440;
            color: #D4D4D4;
            font-size: 17px;
            font-family: "Rajdhani Medium";
            min-width: 500px;
        }
        
        QLineEdit {
            background-color: #1E3440;
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
            border-radius: 20px;
            border: 1px solid #777777;
            padding: 3px;
        }

        QLineEdit:focus {
            border: 1px solid #FF8330;
        }

        QTableCornerButton::section {
            background-color: #181D25;
            color: #D4D4D4;
        }

        QPushButton {
            background-color: #FF5000;
            font-size: 17px;
            font-family: "Rajdhani Medium";
            min-width: 30px;
        }

        QPushButton:disabled {
            background-color: #C17C5C;
            color: #383838;
        }
            
        QScrollBar {
            background-color: #181D25;
        }

        QScrollBar:handle {
            background-color: #000000;
        }

        QTableView {
            font-size: 17px;
            font-family: 'Rajdhani Medium';
            background-color: #3B4252;
            color: #FFFFFF;
        }

        QTableView:selected {
            color: black;
        }

        QMainWindow {
            background-color: #2E3440;
            color: #FF5000;
        }

        QHeaderView {
            font-size: 16px;
            font-family: 'Cantarell Extra Bold';
            background-color: #181D25;
            color: #D4D4D4;
        }

        QLabel {
            font-size: 15px;
            font-family: 'Rajdhani Medium';
            color: #D4D4D4;
        }

        QStatusBar {
            background-color: #000000;
            font-size: 15px;
            font-family: 'Rajdhani Bold';
            color: #D4D4D4;
        }

        QMenuBar, QMenu {
            font-size: 15px;
            font-family: "Rajdhani Medium";
        }

        QMenu {
            background-color: #2E3440;
            color: #D4D4D4;
        }

        QMenu::item:selected {
            color: #FFFFFF;
            background-color: #DE3545;
        }

        QMenuBar {
            background-color: #2E3440;
            color: #D4D4D4;
        }

        QMenuBar::item:selected {
            color: #FFFFFF;
            background-color: #DE3545;
        }

        QMenuBar:selected {
            color: #FFFFFF;
            background-color: #DE3545;
        }

        Search {
            font-size: 15px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
            background-color: #1E3440;
            border: 2px solid red;
            padding: 5px;
            border-radius: 8px;
        }
        )";
    this->setStyleSheet(QString::fromStdString(s));
}
void MainWindow::update_movie_count() {
    movie_count->setText("MOVIES: " + QString::number(model->rowCount()));
}

void MainWindow::init_statusbar()
{
    statusbar_widget = new QWidget();
    statusbar_layout = new QHBoxLayout();
    statusbar_layout->setContentsMargins(0, 0, 0, 0);
    movie_count = new QLabel();
    update_movie_count();
    movie_rating_meter = new QLabel();
    statusbar_layout->addWidget(movie_count);
    statusbar_layout->addWidget(movie_rating_meter);
    statusbar_widget->setLayout(statusbar_layout);
    statusbar->addPermanentWidget(statusbar_widget);
    statusbar->showMessage("Hello World", 3000);
}
void MainWindow::init_menubar()
{
    menubar = new QMenuBar();
    filemenu = menubar->addMenu("&File");
    editmenu = menubar->addMenu("&Edit");
    newAction = filemenu->addAction("New");
    newAction->setShortcut(tr("Ctrl+N"));
    saveAction = filemenu->addAction("Save");
    saveAction->setShortcut(tr("Ctrl+S"));
    saveAsAction = filemenu->addAction("Save As");
    saveAsAction->setShortcut(tr("Ctrl+Shift+S"));
    undoAction = editmenu->addAction("Undo");
    undoAction->setShortcut(tr("Ctrl+Z"));
    redoAction = editmenu->addAction("Redo");
    redoAction->setShortcut(tr("Ctrl+R"));
    prefAction = editmenu->addAction("Preferences");
    prefAction->setShortcut(tr("Ctrl+E"));
    exit = menubar->addAction("Exit");
    connect(exit, &QAction::triggered, this, [this] {
            this->close();
    });
    setMenuBar(menubar);
    db = QSqlDatabase::addDatabase("QSQLITE");
    addmenu = new QMenu();
    single_action = addmenu->addAction("Single");
    multiple_action = addmenu->addAction("Multiple");
    later_action = addmenu->addAction("Watch Later");
    addbtn = new QPushButton("Add");
    addbtn->setMenu(addmenu);
    addmenu->setStyleSheet(QString::fromStdString(R"(
        QMenu {
            background-color: #2E3440;
            color: #D4D4D4;
            font-family: "Rajdhani Medium";
            font-size: 17px;
        }

        QMenu::item:selected {
            color: #FFFFFF;
            background-color: #DE3545;
        }
    )"));
    connect(single_action, &QAction::triggered, this, [this] {
        AddWindow *w = new AddWindow(this);
        w->show();
    });
    connect(multiple_action, &QAction::triggered, this, [this] {
        AddMultipleWindow *w = new AddMultipleWindow(this);
        w->show();
    });
    editbtn = new QPushButton("Edit");
    editbtn->setDisabled(true);
    connect(editbtn, &QPushButton::clicked, this, [this] {
        new EditWindow(this);
    });
    savebtn = new QPushButton("Save");
    connect(savebtn, &QPushButton::clicked, this, &MainWindow::save_all);
    
    deletebtn = new QPushButton("Delete");
    deletebtn->setDisabled(true);
    connect(deletebtn, &QPushButton::clicked, this, &MainWindow::deleteEntry);

    if(!QDir(AppConfDir).exists())
        QDir().mkdir(AppConfDir);
    db.setDatabaseName(AppConfDir + "/mj.sqlite");
    sort_model = new MySortFilterProxyModel();
    main_widget = new QWidget();
    main_layout = new QVBoxLayout();
    search_layout = new QHBoxLayout();
    search_layout->setContentsMargins(10, 2, 10, 2);
    table = new QTableView();
    deselect_table = new QShortcut(QKeySequence("Escape"), table);
    connect(deselect_table, &QShortcut::activated, this, [this] {
        table->selectionModel()->select(table->selectionModel()->currentIndex(), QItemSelectionModel::Clear);
    });
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->verticalHeader()->hide();
    //table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSortingEnabled(true);
    table->resizeColumnsToContents();
    search = new QLineEdit();
    search->setPlaceholderText("Search Movie Name or Rating");
    search->setProperty("id", "Search");
    search->setClearButtonEnabled(true);
    connect(search, &QLineEdit::textChanged, this, &MainWindow::search_text);
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    QMessageBox::StandardButtons m = QMessageBox::question(this, "Exit?", "Are you sure?", QMessageBox::Yes|QMessageBox::No);
    if(m == QMessageBox::Yes)
    {
        model->revertAll();
        this->close();
    }
    else
    {
        e->ignore();
    }
}

void MainWindow::init_table()
{
    if(db.open()) {
        QSqlQuery q(db);
        q.exec("CREATE TABLE IF NOT EXISTS movies (TITLE TEXT NOT NULL, RATE FLOAT)");
        model = new QSqlTableModel();
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        model->setTable("movies");
        connect(model, &QSqlTableModel::primeInsert, this, [this] {
            update_movie_count();
        });
        sort_model->setSourceModel(model);
        sort_model->setFilterCaseSensitivity(Qt::CaseInsensitive);
        table->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
        model->setHeaderData(0, Qt::Horizontal, QObject::tr("Title"));
        model->setHeaderData(1, Qt::Horizontal, QObject::tr("Rating"));
        model->select();
        table->setModel(sort_model);
    }
    else {
        qDebug() << "Cannot open database file";
    }
}

void MainWindow::search_text(QString text)
{
    sort_model->setFilterRegularExpression(text);
}

void MainWindow::save_all()
{
    model->submitAll();
    statusbar->showMessage("Saving Changes", 2000);
    //has_changes = false;
}
Skeleton::Skeleton()
{
    this->setMinimumSize(300, 250);
    this->setMaximumSize(300, 250);
    main_layout = new QGridLayout();
    addbtn = new QPushButton("Add");
    cancel_btn = new QPushButton("Cancel");
    connect(cancel_btn, &QPushButton::clicked, this, [this] {
            this->close();
    });
    review_meter_layout = new QHBoxLayout();
    review_meter = new QLabel("");
    review_meter_layout->addWidget(review_meter, Qt::AlignCenter);
    nowatch = new QCheckBox("Didn't watch it yet");
    title = new QLineEdit();
    rating = new ratingSpinBox();
    rating->setMinimum(0);
    rating->setMaximum(11);
    rating->setSingleStep(0.25);
    title_label = new QLabel("Title:");
    rating_label = new QLabel("Rating:");

    connect(rating, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &EditWindow::review_meter_function);
    main_layout->addWidget(title_label, 0, 0);
    main_layout->addWidget(title, 0, 1, 1, 3);
    main_layout->addWidget(rating_label, 1, 0);
    main_layout->addWidget(rating, 1, 1, 1, 3);
    main_layout->addWidget(nowatch, 2, 1, 1, 3);
    main_layout->addLayout(review_meter_layout, 3, 0, 1, 3);
    main_layout->addWidget(addbtn, 4, 0, 1, 2);
    main_layout->addWidget(cancel_btn, 4, 2, 1, 2);
    setLayout(main_layout);
    styleSheet();
}
void Skeleton::styleSheet()
{
    std::string s = R"(
        QCheckBox {
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
        }
        QLineEdit {
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
        }
        
        QLabel {
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
        }
        
        QDoubleSpinBox {
            color: #D4D4D4;
            font-size: 17px;
            font-family: "Rajdhani Medium";
        }

        QDoubleSpinBox:disabled {
            color: #222222;
            background-color: #555555;
        }

        QDoubleSpinBox:selected {
            border: 1px solid #FF5000;
        }

        QWidget {
            background-color: #2E3440;
        }

        QPushButton {
            font-size: 15px;
            font-family: "Rajdhani Medium";
            background-color: #FF5000;
        }

        QLabel {
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
        }

        )";
    this->setStyleSheet(QString::fromStdString(s));
}
AddWindow::AddWindow(MainWindow *w)
{
    connect(addbtn, &QPushButton::clicked, this, [this, w] {
        add(w);
    });
    connect(nowatch, &QCheckBox::stateChanged, this, [this](int state) {
        if(state == 2) {
            rating->setDisabled(true);
            review_meter->setText("");
            }
        else
            rating->setDisabled(false);
    });
}

void AddWindow::add(MainWindow *w)
{
    QSqlTableModel *m = w->model;
    QSqlRecord r = m->record();
    if(title->text() == "") {
        MessageBox("Title cannot be empty");
        return;
    }
    else
        r.setValue("title", title->text());
    r.setValue("rate", rating->text());
    //has_changes = true;
    if(m->insertRecord(m->rowCount(), r)) {
        w->statusbar->showMessage("Added the Movie Rating", 2500);
        this->close();
    }
    else {
        MessageBox("Couldn't add the given info");
    }
}

void Skeleton::review_meter_function(double d)
{
    if(d == 11)
        review_meter->setText("Masterpiece");
    else if (d > 10)
        review_meter->setText("Exceptional");
    else if(d > 9)
        review_meter->setText("Excellent");
    else if (d > 7.5)
        review_meter->setText("Good");
    else if (d > 5)
        review_meter->setText("Loopholes");
    else if (d > 4.5)
        review_meter->setText("I Got Angry");
    else if (d > 3)
        review_meter->setText("Got up in the middle");
    else if (d > 2)
        review_meter->setText("Could've passed on this movie");
    else if (d > 1)
        review_meter->setText("Why does this movie exist?");
    else if (d == 0)
        review_meter->setText("Look at the rating");
}

EditWindow :: EditWindow(MainWindow *w)
{
    connect(nowatch, &QCheckBox::stateChanged, this, [this](int state) {
        if(state == 2) {
            rating->setDisabled(true);
            review_meter->setText("");
        }
        else
            rating->setDisabled(false);
    });
    M = w->model;
    MM = w->sort_model;
    connect(addbtn, &QPushButton::clicked, this, &EditWindow::add);
    QItemSelectionModel *m = w->table->selectionModel();
    i = m->currentIndex();
    r = M->record(MM->mapToSource(i).row());
    title->setText(r.value("title").toString());
    rating->setValue(r.value("rate").toFloat());
    this->show();
}

void EditWindow::add()
{
    QSqlRecord a = M->record();
    a.setGenerated("title", false);
    a.setGenerated("rate", true);
    if(nowatch->isChecked())
        a.setValue("rate", "");
    else
        a.setValue("rate", rating->text().toFloat());

    a.setValue("title", title->text());
    M->setRecord(MM->mapToSource(i).row(), a);
    //has_changes = true;
    this->close();
}

void MainWindow::deleteEntry()
{
    QItemSelectionModel *d = table->selectionModel();
    QSortFilterProxyModel *MM = sort_model;
    QModelIndexList i = d->selectedRows();
    QMessageBox::StandardButtons m = QMessageBox::question(this, "Delete", "Are you sure about deleting " + QString::number(i.size()) + " members ?", QMessageBox::Yes|QMessageBox::No);
    if(m == QMessageBox::Yes)
    foreach(QModelIndex r, i) {
        model->removeRow(MM->mapToSource(r).row());
    }
    statusbar->showMessage("Successfully deleted", 2000);
    update_movie_count();
}
AddMultipleWindow::AddMultipleWindow(MainWindow *w)
{
    done_btn = new QPushButton("Done");
    cancel_btn = new QPushButton("Cancel");
    addrowBtn = new QPushButton("Add Row");
    delrowBtn = new QPushButton("Delete Row");
    mult_table = new QTableWidget();
    mult_table->setColumnCount(3);
    mult_table->setHorizontalHeaderLabels({"Title", "Rating", "Review"});
    mult_table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    mult_table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    mult_table->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    mult_table->setRowCount(1);
    ratingSpinBox *r = new ratingSpinBox();
    r->setProperty("row", mult_table->rowCount()-1);
    QLineEdit *s = new QLineEdit("");
    s->setDisabled(true);
    connect(r, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this, r](double d) {
            review_meter_function(r, d);
    });
    mult_table->setCellWidget(0, 1, r);
    mult_table->setCellWidget(0, 2, s);
    btnlayout = new QHBoxLayout();
    btnlayout->addWidget(addrowBtn);
    connect(addrowBtn, &QPushButton::clicked, this, [this] {
        int rc = mult_table->rowCount();
        rc++;
        mult_table->setRowCount(rc);
        ratingSpinBox *s = new ratingSpinBox();
        s->setProperty("row", rc-1);
        connect(s, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this, s](double d) {
                review_meter_function(s, d);
        });
        mult_table->setCellWidget(rc-1, 1, s);
        QLineEdit *tt = new QLineEdit();
        tt->setDisabled(true);
        mult_table->setCellWidget(rc-1, 2, tt);
    });
    btnlayout->addWidget(delrowBtn);
    connect(delrowBtn, &QPushButton::clicked, this, [this] {
        mult_table->setRowCount(mult_table->rowCount()-1);
    });
    btnlayout->addWidget(done_btn);
    connect(done_btn, &QPushButton::clicked, this, [this, w] {
        AddMultipleWindow::done(w);
    });
    btnlayout->addWidget(cancel_btn);
    connect(cancel_btn, &QPushButton::clicked, this, [this] {
        this->close();
    });
    main_layout = new QVBoxLayout();
    main_layout->addWidget(mult_table);
    main_layout->addLayout(btnlayout);
    setLayout(main_layout);
    styleSheet();
}

void AddMultipleWindow::styleSheet() {
    std::string s = R"(
        QWidget {
            background-color: #2E3440;
        }

        QPushButton {
            background-color: #FF5000;
            font-size: 17px;
            font-family: "Rajdhani Medium";
            min-width: 30px;
        }

        QPushButton:disabled {
            background-color: #C17C5C;
            color: #383838;
        }
            
        QScrollBar {
            background-color: #181D25;
        }

        QScrollBar:handle {
            background-color: #000000;
        }
        
        QLineEdit:focus {
            border: 1px solid #FF8330;
        }
        
        QLineEdit {
            background-color: #1E3440;
            font-size: 17px;
            font-family: "Rajdhani Medium";
            color: #D4D4D4;
            border-radius: 20px;
            border: 1px solid #777777;
            padding: 3px;
        }
        
        QPushButton:disabled {
            background-color: #C17C5C;
            color: #383838;
        }
        
        QHeaderView {
            font-size: 16px;
            font-family: 'Cantarell Extra Bold';
            background-color: #181D25;
            color: #D4D4D4;
        }

        QTableWidget {
            font-size: 17px;
            font-family: 'Rajdhani Medium';
            background-color: #3B4252;
            color: #FFFFFF;
        }

        QTableWidget:selected {
            color: black;
        }
        
        QTableCornerButton::section {
            background-color: #181D25;
            color: #D4D4D4;
        }

        QDoubleSpinBox {
            color: #D4D4D4;
            font-size: 17px;
            font-family: "Rajdhani Medium";
        }
        
        QDoubleSpinBox:selected {
            border: 1px solid #FF5000;
        }
        )";
    this->setStyleSheet(QString::fromStdString(s));
}

void AddMultipleWindow::done(MainWindow *w)
{
    m = w->model;
    titles.clear();
    ratings.clear();
    QSqlRecord r = m->record();
    for(int i=0; i<mult_table->rowCount(); i++)
    {
        QTableWidgetItem *itm = mult_table->item(i, 0);
        if(itm)
            titles.append(itm->text());
        else
            titles.append("");
        ratings.append(mult_table->cellWidget(i, 1)->property("text").toString());
    }
    for(int i=0; i<titles.size(); i++)
    {
        r.setValue("title", titles.at(i));
        r.setValue("rate", ratings.at(i).toDouble());
        m->insertRecord(m->rowCount(), r);
    }

}
void AddMultipleWindow::review_meter_function(ratingSpinBox *r, double d)
{   
    QLineEdit *c = qobject_cast<QLineEdit*>(mult_table->cellWidget(r->property("row").toInt(), 2));
    if(d == 11)
        c->setText("Masterpiece");
    else if (d > 10)
        c->setText("Exceptional");
    else if(d > 9)
        c->setText("Excellent");
    else if (d > 7.5)
        c->setText("Good");
    else if (d > 5)
        c->setText("Loopholes");
    else if (d > 4.5)
        c->setText("I Got Angry");
    else if (d > 3)
        c->setText("Got up in the middle");
    else if (d > 2)
        c->setText("Could've passed on this movie");
    else if (d > 1)
        c->setText("Why does this movie exist?");
    else if (d == 0)
        c->setText("Look at the rating");
}
#endif

